provider "aws" {
  region = var.region
}

# data "aws_availability_zones" "available" {}

locals {
  cluster_name = "ge-poc-eks"
}

#resource "random_string" "suffix" {
#  length  = 8
#  special = false
#}

#module "endpoints" {
#  source = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  
#  name = "pocvpc"
#  vpc_id = "vpc-0f56689e0bc065b50"


#module "vpc" {
#  source  = "terraform-aws-modules/vpc/aws"
#  version = "3.19.0"

#  name = "pocvpc"

#  cidr = "10.16.0.0/16"
#  azs  = slice(data.aws_availability_zones.available.names, 0, 3)

#  subnet_ids = ["subnet-000f5817dbfa7041f", "subnet-00701e20e57eac990", "subnet-09669be7cc93b0d39"]
#  public_subnets  = ["subnet-0acee207da506aa58", "subnet-06cce8db631d6ca9a", "subnet-0ad98e4fdc41da4dc"]

#  enable_nat_gateway   = true
#  single_nat_gateway   = true
#  enable_dns_hostnames = true

#  public_subnet_tags = {
#    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
#    "kubernetes.io/role/elb"                      = 1
#  }

#  private_subnet_tags = {
#    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
#    "kubernetes.io/role/internal-elb"             = 1
#  }
#}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "19.5.1"

  cluster_name    = local.cluster_name
  cluster_version = "1.24"

  vpc_id    = var.vpc_id
  subnet_ids = var.subnet_id
  cluster_endpoint_public_access = true

  eks_managed_node_group_defaults = {
    ami_type = "AL2_x86_64"

  }

  eks_managed_node_groups = {
    one = {
      name = "eksworker"

      instance_types = ["t3.2xlarge"]

      min_size     = 2
      max_size     = 4
      desired_size = 2
    }

  #  two = {
  #    name = "node-group-2"

  #    instance_types = ["t3.small"]

  #    min_size     = 1
  #    max_size     = 2
  #    desired_size = 1
  #  }
  }
}