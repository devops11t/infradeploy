#------------------------------------------------------------------------------#
# Required variables
#------------------------------------------------------------------------------#

variable "vpc_id" {
  type        = string
  description = "ID of an existing VPC in which to create the cluster."
}

variable "subnet_id" {
  type        = list(string)
  description = "ID of an existing subnet in which to create the cluster. The subnet must be in the VPC specified in the \"vpc_id\" variable, otherwise an error occurs."
}

#------------------------------------------------------------------------------#
# Optional variables
#------------------------------------------------------------------------------#

variable "region" {
  description = "AWS region"
  type        = string
  default     = "us-east-1"
}
